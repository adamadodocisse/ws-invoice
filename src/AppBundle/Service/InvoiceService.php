<?php

namespace AppBundle\Service;

use GuzzleHttp\Client;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

/**
 * Gestionnaire de facture.
 */
class InvoiceService
{
    /**
     * @var array
     */
    private $parameters;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters = array())
    {
        $this->parameters = $parameters;
    }

    /**
     * Liste des factures en fonction des critères de recherche
     *
     * @param FormInterface $filterForm
     * @return array
     */
    public function getInvoices(FormInterface $filterForm)
    {
        $criteria = $filterForm->getData();
        $client = new Client(['timeout' => 0.5]);
        $results = [
            'errors' => [],
            'invoices' => []
        ];
        $query = $this->buildQueryString($criteria);

        try {
            $response = $client->get($this->getInvoicesEndpoint() . '?' . $query);

            if ($response->getStatusCode() === 200) {
                $content = $response->getBody()->getContents();
                $results =  $this->formatWSResponse($this-> wsContentToArray($content));
                $this->checkInvoicesFormFilterErrors($filterForm, $results['errors']);
            }
        } catch(\Exception $e) {}

        return $results;
    }

    /**
     * Transforme un tableau clé valeur au format query.
     *
     * @param array $criteria le tableau
     * @return string
     */
    private function buildQueryString(array $criteria)
    {
        $query = [];

        foreach ($criteria as $key => $value) {
            if ($value instanceof \DateTime) {
                $v = $value->format('Y-m-d');
            } else {
                $v = $value;
            }

            $query[] = "$key=$v";
        }

       return empty($query) ? '' : join('&', $query);
    }

    /**
     * @param string $content contenu de la page.
     * @return array
     */
    private function wsContentToArray($content)
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        $dom->loadHTML($content);
        $trsNodes = $dom->getElementsByTagName('tr');
        $headerNodes = $dom->getElementsByTagName('th');
        $headers = [];

        for ($i = 0; $i < $headerNodes->length; $i++) {
            $headers[] = trim(utf8_decode($headerNodes->item($i)->textContent));
        }

        $rows = $this->buildRows($trsNodes, $headers, 1, function($key, $value) {
            if (preg_match("/Date.*/", $key)) {
                $value = \DateTime::createFromFormat('Y-m-d', $value);
            }
            return $value;
        });

        return $rows;
    }

    /**
     * Formattage de la reponse renvoyée par le ws.
     *
     * @param array $rows les tableaux contenant les factures ou les messages d'erreurs du WS.
     * @return array
     */
    private function formatWSResponse(array $rows)
    {
        $results = [
            'errors' => [],
            'invoices' => []
        ];

        $results['invoices'] = $rows;

        /**
         * Verification des erreurs.
         */
        foreach ($rows as $row) {
            if (isset($row['Champ erroné'])) {
                $results['invoices'] = [];
                $results['errors'][] = $row['Champ erroné'];
            }
        }
        /**
         * On ajoute le lien de téléchargement pour chaque facture.
         */
        $results['invoices'] = array_map(function($invoice) {
            $invoice = array_values($invoice);
            $invoice[count($invoice)] = str_replace(
                ['{customer_id}', '{invoice_id}'],
                [$invoice[0], $invoice[2]],
                $this->getInvoiceDownloadEndpoint()
            );

            return $invoice;
        }, $results['invoices']);

        return $results;
    }

    /**
     *
     * Création d'une liste de ligne à partir d'une liste de noeud de trs.
     *
     * @param \DOMNodeList $trsNodes
     * @param array $headers L'entête.
     * @param int $index position à la quelle commence le parcours de la liste.
     * @param callable $valueTransformer transforme une valeur en fonction de son entête.
     * @return array
     */
    private function buildRows(\DOMNodeList $trsNodes, array $headers, $index, callable $valueTransformer = null)
    {
        $rows = [];

        for ($i = $index; $i < $trsNodes->length; $i++) {
            $tdsNodes = $trsNodes->item($i)->childNodes;
            $rows[] = $this->buildRow($tdsNodes, $headers, $valueTransformer);
        }

        return $rows;
    }

    /**
     * Création d'une ligne à partir d'une liste de noeud de td.
     *
     * @param \DOMNodeList $tdsNodes La liste des tds.
     * @param array $headers L'entête.
     * @param callable|null $valueTransformer transforme une valeur en fonction de son entête.
     * @return array
     */
    private function buildRow(\DOMNodeList $tdsNodes, array $headers, callable $valueTransformer = null)
    {
        $values = [];

        for ($j = 0; $j < $tdsNodes->length; $j++) {
            $key = $headers[$j];
            $value = trim(utf8_decode($tdsNodes->item($j)->textContent));
            $values[$key] = $valueTransformer ? call_user_func_array($valueTransformer, [$key, $value]) : $value;
        }

        return $values;
    }

    /**
     * Verification des erreurs dans le formulaire de filtrage de facture.
     *
     * @param FormInterface $filterForm
     * @param $errors
     */
    private function checkInvoicesFormFilterErrors(FormInterface $filterForm, array $errors)
    {
        foreach ($errors as $errorKey) {
            $errorKey = strtolower($errorKey);
            if ($filterForm->has($errorKey)) {
                $filterForm->get($errorKey)->addError(new FormError("Cette valeur n'est pas valide."));
            }
        }
    }

    /**
     * Endpoint de la liste des factures.
     *
     * @return mixed
     */
    private function getInvoicesEndpoint()
    {
        return $this->parameters['endpoint']['list'];
    }

    /**
     * Endpoint de téléchargement d'une facture.
     *
     * @return mixed
     */
    private function getInvoiceDownloadEndpoint()
    {
        return $this->parameters['endpoint']['download'];
    }
}
