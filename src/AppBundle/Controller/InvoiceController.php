<?php

namespace AppBundle\Controller;

use AppBundle\Form\Type\InvoiceFilterType;
use AppBundle\Service\InvoiceService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class InvoiceController
 *
 */
class InvoiceController extends Controller
{
    /**
     * Page de listing des facture.
     *
     * @Template()
     *
     * @Route("/", name="app_invoice_list")
     * @param Request $request Gestionnaire de requête.
     * @param InvoiceService $invoiceService Gestionnaire de facture.
     * @return array
     */
    public function listAction(Request $request, InvoiceService $invoiceService)
    {
        $form = $this->createForm(InvoiceFilterType::class, [], ['method' => 'GET']);

        if ($form->handleRequest($request)->isSubmitted() && $form->isValid()) {
            $results = $invoiceService->getInvoices($form);
            $invoices = $results['invoices'];
        } else {
            $invoices = array();
        }

        return array(
            'form' => $form->createView(),
            'invoices' => $invoices
        );
    }
}
