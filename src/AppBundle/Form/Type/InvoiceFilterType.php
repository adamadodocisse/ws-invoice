<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * Formulaire de filtrage des factures.
 */
class InvoiceFilterType extends AbstractType
{
    /**
     * Valeur par defaut du nombre total de facture.
     */
    const DEFAULT_TOTAL_BILLS = 10;

    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('profile', ChoiceType::class, [
                'label' => 'Type de profile :',
                'choices' => [
                    'ADV' => 'ADV',
                    'CLIENT' => 'CLIENT'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('serial_number', HiddenType::class, [
                'data' => uniqid()
            ])
            ->add('customer_id', TextType::class, [
                'label' => 'Numéro client :',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('order_no', TextType::class, [
                'label' => 'Numéro de contrat :',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('customer_email', TextType::class, [
                'label' => 'Email du client :',
                'constraints' => [
                    new NotBlank(),
                    new Email()
                ]
            ])
            ->add('total_bills', NumberType::class, [
                'label' => 'Nombre total de factures :',
                'data' => self::DEFAULT_TOTAL_BILLS,
                'constraints' => [
                    new NotBlank(),
                    new Regex(['pattern' => '/^[0-9]*[1-9]+[0-9]*$/'])
                ]
            ])
            ->add('search_start_date', DateType::class, [
                'label' => 'Date de debut :',
                'widget' => 'single_text',
                'constraints' => [
                    new Date()
                ]
            ])
            ->add('search_end_date', DateType::class, [
                'label' => 'Date de fin :',
                'widget' => 'single_text',
                'constraints' => [
                    new Date()
                ]
            ])
        ;
    }

    /**
     * @inheritdoc
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
